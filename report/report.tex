\documentclass{tnreport}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{Programmation et Vérification d’Algorithmes Répartis} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{lipsum}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{listings}
\usepackage[absolute,overlay]{textpos}

\usetikzlibrary{arrows.meta}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage

\chapter{Introduction}

Ce présente notre travail réalisé dans le cadre du projet d'ASPD. Il a pour objectif de nous faire découvrir des outils de preuves de programmes et d'algorithmes distribués.\\
\\
Pour commencer, nous avons dû implémenter l'algorithme de Carvalho et Roucairol en \textbf{TLA+} en nous aidant de celui de Ricart et Agrawala. Ensuite, nous avons analysé et résumé l'article \textbf{Distributed Algorithms for Sensor Networks}. Pour finir, nous avons implementé un algorithme qui permet de multiplier deux matrices en \textbf{TLA+} à l'aide de \textbf{PlusCal} puis en \textbf{DistAlgo}.

\chapter{Algorithme de Carvalho et Roucairol}

L'algorithme de Carvalho et Roucairol est une amélioration de celui proposé par Ricart et Agrawala. C'est un algorithme d'exclusion mutuelle utilisé dans le cadre de systèmes distribués.\\
\\
L'amélioration consiste à permettre à un site d'accéder de nouveau à la section critique sans demander l'autorisation si, depuis son dernier accès, aucun autre site a demandé l'accès.\\
\\
L'annexe \ref{annexe:carvalho_et_roucairol} reprend l'algorithme de  Ricart et Agrawala modifié pour implémenter celui de Carvalho et Roucairol.


\chapter{Analyse : Distributed Algorithms for Sensor Networks}

\section{Introdution}

De nos jours, il est possible de trouver un nombre de plus en plus important de systèmes distribués. Ils sont utilisés dans divers domaines tels que le peer-to-peer, le cloud computing et également internet.

Pour tous ces systèmes, il est nécessaire de diviser la charge de calcul sur plusieurs machines. De ce fait, cela réduit le temps de calcul et les besoins en espace mémoire. Par ailleurs, ces machines ont un degré de liberté important. En effet, chacune a son propre matériel (mémoire, CPU,...) ainsi que son propre code et données. Parfois, elles peuvent également effectuer des tâches indépendantes.

Un réseau de capteur peut être représenté par un ensemble de nœuds, où chaque nœud représente un micro-ordinateur qui va recevoir des données de ses capteurs et qui est capable de communiquer aux autres micro-ordinateurs du graphe par fréquence radio. Les algorithmes distribués sont les fondations théoriques des systèmes distribués.

Il existe deux types de tâche : les tâches globales et celles locales. Pour illustrer nos propos, nous pouvons prendre l'exemple d'un calcul de température minimale dans un bâtiment. Une tâche globale viserait à obtenir la température de tous les capteurs et à déterminer le minimum de toutes les mesures. Dans le cas d'une tâche locale, les micro-ordinateurs seraient amenés à calculer la température minimale entre celles de leurs voisins et la leur. Ils vont ensuite transmettre cette température aux autres capteurs.

Les avantages des tâches locales sont l'utilisation d'une faible mémoire, d'une rapidité augmentée et d'une efficacité accrue puisque les calculs sont répartis et permettent donc le transfert de données. De même, si un micro-ordinateur du graphe est corrompu pour une tâche locale, il ne bloquera pas tout le système.

Dans la suite de ce rapport, nous allons étudier les limites de ces algorithmes et mettre en exergue ce qui est impossible de réaliser avec un système distribué.

\clearpage

\section{Algorithmes distribués}

Pendant de longues années, les algorithmes distribués ont été analysés d'une façon totalement théorique. Cependant, ces algorithmes, conçus avec des conditions pures, ont rencontré des problèmes lors de leur exécution dans la réalité.\\
\\\\
Voici le squelette général d'un algorithme distribué :

\begin{algorithm}
	\caption{Squelette général d'un algorithme distribué }
	\hbox{\textit{Chaque nœud réalise les actions suivantes}}
	\begin{algorithmic}
		\REPEAT 
		\STATE Envoyer des messages aux voisins
		\STATE Recevoir des messages des voisins
		\STATE Réaliser différentes actions pour la prochaine étape
		\UNTIL{fin}
	\end{algorithmic}
\end{algorithm}

Lorsqu'on étudie les algorithmes distribués, un critère important est la durée d'exécution du programme. Nous allons donc étudier le problème d'indexation maximale (MIS). En effet, si plusieurs nœuds voisins souhaitent envoyer un message en même temps, il va y avoir des interférences. Pour limiter ce problème, nous allons essayer d'identifier des nœuds indépendants.\\

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{figures/graph}
	\label{fig:grapg}
\end{figure}

Sur l'image ci-dessus, les nœuds en noir sont indépendant les uns des autres, comme les nœuds gris et ceux en blanc.

\begin{algorithm}[ht!]
	\caption{MIS naïf}
	\hbox{\textit{Entrée : tous les nœuds v avec un identifiant unique}}
	\begin{algorithmic}
		\STATE{Envoie son ID aux autres nœuds}
		\STATE{Reçoit l'ID des autres nœuds}
		\STATE{Enregistre les voisins avec la plus haute priorité tel que $A = \{ w \in Nv | id(w) > id(v)\}$}
		\REPEAT 
		\IF{A = $\emptyset$}
		\STATE Envoyer 'rejoint' aux voisins
		\STATE Rejoindre le MIS et terminer
		\ENDIF
		\IF{reçois 'rejoint'}
		\STATE Envoyer 'ne rejoint pas' aux voisins
		\STATE Ne pas rejoindre le MIS et terminer
		\ENDIF
		\STATE Recevoir 'ne rejoint pas' des voisins
		\UNTIL{fin}
	\end{algorithmic}
\end{algorithm}

L'algorithme MIS naïf reçoit tous les nœuds avec un identifiant unique. Ensuite, chaque nœud compare son ID avec celui de ses voisins et s'il a le plus grand ID, il rejoint le MIS.\\
Cependant, dans la pratique, le graphe peut être très large avec un diamètre important, et cet algorithme peut prendre beaucoup de temps.\\ 
Nous allons donc essayer de trouver un algorithme plus efficace.\\
\\
Une solution plus rapide pourrait être de déterminer les ID de façon aléatoire. En effet, cela éviterait à chaque nœud de devoir envoyer son ID à tous les autres nœuds. Ici, seuls ses voisins seraient notifiés de son ID. En réduisant le nombre de messages, on augmente la vitesse d'exécution du programme.

\begin{algorithm}[ht!]
	\caption{MIS aléatoire}
	\begin{algorithmic}
		\STATE{Détermine son ID de façon aléatoire}
		\STATE{Envoie son ID aux nœuds voisins}
		\STATE{Reçoit l'ID des nœuds voisins}
		\REPEAT 
		\IF{l'ID du nœud est plus grand que ses voisins}
		\STATE Envoyer 'rejoint' à tous les nœuds
		\STATE Rejoindre le MIS et terminer
		\ENDIF
		\IF{reçois 'rejoint'}
		\STATE Envoyer 'ne rejoint pas' à tous les nœuds
		\STATE Ne pas rejoindre le MIS et terminer
		\ENDIF
		\STATE Recevoir 'ne rejoint pas' des voisins
		\UNTIL{fin}
	\end{algorithmic}
\end{algorithm}

\section{Connectivité sans fil et interférences}

Dans la pratique, nous pourrions être amenés à réaliser des réseaux de capteurs reliés entre eux en utilisant un réseau non filaire. Cependant, plusieurs contraintes vont compliquer ce travail. En effet, une connexion sans fil est limitée dans l'espace : si deux capteurs sont trop éloignés ou si un obstacle se trouve sur leur chemin, les deux nœuds ne pourront pas communiquer.\\
\\
De plus, dans un réseau sans fil, les capteurs communiquent le plus souvent par radio. Si deux ou plusieurs nœuds émettent en même temps, cela va provoquer des interférences.

\section{Horloge de synchronisation}

Précédemment, nous avons évoqué les problèmes de collisions lorsque deux capteurs souhaitent transmettre un message.\\
\\
Pour éviter ce problème, nous pourrions mettre en place un système d'horloge. Ainsi, en utilisant l'adresse MAC de chaque capteur, nous pourrons allouer à tous les nœuds une période pendant laquelle il pourra transmettre des messages.\\
\\
Cependant, celle solution a un coût énergétique important puisque chaque capteur devra rester éveiller pour pouvoir communiquer dans la période dédiée.

\section{Conclusion}

L'étude de la complexité des algorithmes distribués pose de nombreuses questions. Dans le cas des réseaux de capteurs, les recherches restent encore théoriques mais des cas pratiques tendent à apparaître. Nous avons essentiellement étudié le pouvoir des algorithmes distribués vis-à-vis de la coordination de tâches.

\chapter{Multiplication de deux matrices}

Dans le cadre de ce projet, la dernière tâche que nous devions réaliser est la réalisation de deux programmes via deux outils différents. La première sous-partie s'attache à présenter le premier outil utilisé, à savoir la plateforme \textbf{TLA+} permettant de faire de la preuve de programme et donc de s'assurer du bon fonctionnement de l'algorithme associé. La seconde concerne la langage \textbf{DistAlgo}. Toutefois, les deux programmes écrits ont pour but de multiplier deux matrices quelconques en respectant les contraintes mathématiques associées. 

\section{Modélisation en TLA+}

Comme évoqué précédemment, le premier programme a été codé via l'utilisation de la plateforme \textbf{TLA}. Nous avons réussi à réaliser la multiplication de deux matrices en utilisant le langage \textbf{PlusCal}. Ce langage permet de déterminer plus facilement les conditions de sûreté et de fiabilité d'un programme. L'annexe \ref{annexe:programme_tla_matrices} montre le programme auquel nous avons abouti pour réaliser la tâche demandée. 

Pour cette partie, l'idée est de définir trois processus. Le processus A est chargé de lire les lignes d'une matrice et de le notifier au processus \textit{B} qui est chargé de lire les colonnes. Une fois que B a terminé la lecture d'une des colonnes, il notifie le processus \textit{A} pour qu'il lise la ligne suivante et le processus \textit{C} pour qu'il se charge de multiplier la valeur d'une ligne d'une matrice \textit{X} à la valeur d'une colonne d'une matrice \textit{Y}. 

Notre programme a pu être vérifié par la définition d'un modèle vérifiant les conditions de sûreté et le non-blocage au moment de l'exécution. 

\section{Programmation en DistAlgo}

Dans un second temps, nous avons retranscrit ce principe à un programme écrit sous \textbf{Python} via la librairie \textbf{DistAlgo}. Cette librairie permet d'écrire des programmes distribués, c'est-à-dire des programmes exécutant des instructions sur différents sites (processus) en leur permettant de communiquer entre eux. L'annexe \ref{annexe:programme_da_matrices} présente le travail réalisé grâce à cet outil. 

Comme précédemment, l'idée consiste à définir différents processus qui doivent exécuter des tâches qui leur sont propres. Cependant, ils sont au nombre de quatre pour cette version. Un processus \textit{PA} se charge de lire les lignes d'une matrice, le processus \textit{PB} de lire les colonnes, le processus \textit{CALCUL} de faire la multiplication en fonction des valeurs lues par \textit{PA} et \textit{PB} et enfin le processus \textit{PC} qui affiche le résultat lorsque tous les calculs ont été effectués.

Notre programme a été vérifié suite à de multiples exécutions de ce dernier avec différents jeux de données. La figure \ref{fig:process_da_matrices} récapitule les actions effectuées chronologiquement pour afficher le résultat, correspondant à la multiplication de deux matrices quelconques. 

\bigskip

\begin{figure}[ht!]
	\begin{center}
		\begin{tikzpicture}
			\begin{scope}[every node/.style={circle,thick,draw}]
				\node (PA) at (0,0) {PA};
				\node (PB) at (3,0) {PB};
				\node (CALCUL) at (1.5,-3) {CALCUL};
				\node (PC) at (1.5,-6) {PC};
			\end{scope}
			
			\begin{scope}[>={Stealth[black]},
			every node/.style={fill=white,circle},
			every edge/.style={draw=red,very thick}]
				\path [->] (PA) edge[bend right=40] node {$2$} (PB);
				\path [->] (PA) edge[bend right=15] node {$1$} (CALCUL);
				\path [->] (PB) edge[bend right=40] node {$4$} (PA);
				\path [->] (PB) edge[bend left=15] node {$3$} (CALCUL);
				\path [->] (CALCUL) edge node {$5$} (PC);
			\end{scope}
		\end{tikzpicture}
		\begin{textblock*}{5cm}(14cm,8cm) % {block width} (coords) 
			\begin{flushleft}
				\scriptsize
				\textbf{1} : Envoie ligne\\
				\textbf{2} : Notifie ligne\\
				\textbf{3} : Envoie colonne\\
				\textbf{4} : Notifie colonne\\
				\textbf{5} : Envoie résultat
			\end{flushleft}
		\end{textblock*}
		\bigskip
		\caption{Actions effectuées en DistAlgo pour la multiplication de deux matrices}
		\label{fig:process_da_matrices}
	\end{center}
\end{figure}

\clearpage

\listoffigures

\appendix
\part*{Annexes}
\addcontentsline{toc}{part}{Annexes}

\chapter{Carvahlo et Roucairol - TLA+}
\label{annexe:carvalho_et_roucairol}
\begin{center}
	\lstinputlisting[language=C,basicstyle=\scriptsize]{../Tache_1/w1.tla}
\end{center}

\chapter{Multiplication de deux matrices - TLA+}
\label{annexe:programme_tla_matrices}
\begin{center}
	\lstinputlisting[language=C,basicstyle=\scriptsize]{../Tache_3/d2.tla}
\end{center}

\chapter{Multiplication de deux matrices - DistAlgo}
\label{annexe:programme_da_matrices}
\begin{center}
	\lstinputlisting[language=Python,basicstyle=\scriptsize]{../Tache_3/d2.da}
\end{center}

\end{document}
