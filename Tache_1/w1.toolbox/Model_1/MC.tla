---- MODULE MC ----
EXTENDS w1, TLC

\* CONSTANT definitions @modelParameterConstants:0SITES
const_15586116906362000 == 
{1,2,3}
----

\* CONSTANT definitions @modelParameterConstants:1N
const_15586116906463000 == 
3
----

\* CONSTANT definitions @modelParameterConstants:2NUM
const_15586116906564000 == 
{1,2,3}
----

\* INIT definition @modelBehaviorInit:0
init_15586116906665000 ==
Init
----
\* NEXT definition @modelBehaviorNext:0
next_15586116906776000 ==
SafeNext
----
=============================================================================
\* Modification History
\* Created Thu May 23 13:41:30 CEST 2019 by ophelien
