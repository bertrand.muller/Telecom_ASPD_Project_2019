-------------------------------- MODULE d2  --------------------------------
EXTENDS TLC,Integers,Naturals
CONSTANTS A, B, N, M, P

(*
--fair algorithm multiplication_matrices
  variables sendA = [l \in 1..N |-> [c \in 1..M |-> 0]],
            sendB = [l \in 1..P |-> [c \in 1..N |-> 0]],
            sendC = [l \in 1..N*P |-> 0],
            C     = [l \in 1..N |-> [c \in 1..P |-> 0]],
            produit, 
            finished = 0
    
  process PA \in 1..N
  begin
    send: sendA[self] := A[self];
  end process;
    
  process PB \in N+1..N+P 
    variables lB, colB = [l \in 1..P |-> 0]
  begin
    init: lB := 1;
    whil: while lB <= N do
      affe: colB[lB] := B[lB][self-N];
      incr: lB := lB+1;
    end while;
    send: sendB[self-N] := colB;
  end process;

  process CALCUL \in N+P+1..N+P+N*P
    variables i = (self-N-P+1) \div N, j = ((self-N-P)%N)+1, result = 0, h = 1
  begin
    wait: await {a \in 1..M : sendA[i][a] = 0} = {} /\ {a \in 1..M : sendB[j][a] = 0} = {};
    whil: while h <= M do
      calc: result := result + sendA[i][h]*sendB[j][h];
      incr: h := h+1;
    end while;
    affe: sendC[(i-1)*N+j] := result;
  end process;

  process PC \in N+P+N*P+1..N+P+N*P*2
    variables i = (self-N-P-N*P+1) \div N, j = ((self-N-P-N*P)%N)+1
  begin
    wait: await sendC[(i-1)*N+j] # 0;
    affe: C[i][j] := sendC[(i-1)*N+j];s
    wend: await {a \in 1..N*P : C[(a+1) \div 2][((a+1)%2)+1] = 0} = {};
    fini: finished := 1;
  end process;
 
  process PRINT=0
  begin
    wait: await finished=1;
    disp: print <<C>>;
  end process;
    
end algorithm;
*)
\* BEGIN TRANSLATION
\* Label send of process PA at line 16 col 11 changed to send_
\* Label whil of process PB at line 23 col 11 changed to whil_
\* Label affe of process PB at line 24 col 13 changed to affe_
\* Label incr of process PB at line 25 col 13 changed to incr_
\* Label wait of process CALCUL at line 33 col 11 changed to wait_
\* Label affe of process CALCUL at line 38 col 11 changed to affe_C
\* Label wait of process PC at line 44 col 11 changed to wait_P
\* Process variable i of process CALCUL at line 31 col 15 changed to i_
\* Process variable j of process CALCUL at line 31 col 40 changed to j_
CONSTANT defaultInitValue
VARIABLES sendA, sendB, sendC, C, produit, finished, pc, lB, colB, i_, j_, 
          result, h, i, j

vars == << sendA, sendB, sendC, C, produit, finished, pc, lB, colB, i_, j_, 
           result, h, i, j >>

ProcSet == (1..N) \cup (N+1..N+P) \cup (N+P+1..N+P+N*P) \cup (N+P+N*P+1..N+P+N*P*2) \cup {0}

Init == (* Global variables *)
        /\ sendA = [l \in 1..N |-> [c \in 1..M |-> 0]]
        /\ sendB = [l \in 1..P |-> [c \in 1..N |-> 0]]
        /\ sendC = [l \in 1..N*P |-> 0]
        /\ C = [l \in 1..N |-> [c \in 1..P |-> 0]]
        /\ produit = defaultInitValue
        /\ finished = 0
        (* Process PB *)
        /\ lB = [self \in N+1..N+P |-> defaultInitValue]
        /\ colB = [self \in N+1..N+P |-> [l \in 1..P |-> 0]]
        (* Process CALCUL *)
        /\ i_ = [self \in N+P+1..N+P+N*P |-> (self-N-P+1) \div N]
        /\ j_ = [self \in N+P+1..N+P+N*P |-> ((self-N-P)%N)+1]
        /\ result = [self \in N+P+1..N+P+N*P |-> 0]
        /\ h = [self \in N+P+1..N+P+N*P |-> 1]
        (* Process PC *)
        /\ i = [self \in N+P+N*P+1..N+P+N*P*2 |-> (self-N-P-N*P+1) \div N]
        /\ j = [self \in N+P+N*P+1..N+P+N*P*2 |-> ((self-N-P-N*P)%N)+1]
        /\ pc = [self \in ProcSet |-> CASE self \in 1..N -> "send_"
                                        [] self \in N+1..N+P -> "init"
                                        [] self \in N+P+1..N+P+N*P -> "wait_"
                                        [] self \in N+P+N*P+1..N+P+N*P*2 -> "wait_P"
                                        [] self = 0 -> "wait"]

send_(self) == /\ pc[self] = "send_"
               /\ sendA' = [sendA EXCEPT ![self] = A[self]]
               /\ pc' = [pc EXCEPT ![self] = "Done"]
               /\ UNCHANGED << sendB, sendC, C, produit, finished, lB, colB, 
                               i_, j_, result, h, i, j >>

PA(self) == send_(self)

init(self) == /\ pc[self] = "init"
              /\ lB' = [lB EXCEPT ![self] = 1]
              /\ pc' = [pc EXCEPT ![self] = "whil_"]
              /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, colB, 
                              i_, j_, result, h, i, j >>

whil_(self) == /\ pc[self] = "whil_"
               /\ IF lB[self] <= N
                     THEN /\ pc' = [pc EXCEPT ![self] = "affe_"]
                     ELSE /\ pc' = [pc EXCEPT ![self] = "send"]
               /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                               colB, i_, j_, result, h, i, j >>

affe_(self) == /\ pc[self] = "affe_"
               /\ colB' = [colB EXCEPT ![self][lB[self]] = B[lB[self]][self-N]]
               /\ pc' = [pc EXCEPT ![self] = "incr_"]
               /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                               i_, j_, result, h, i, j >>

incr_(self) == /\ pc[self] = "incr_"
               /\ lB' = [lB EXCEPT ![self] = lB[self]+1]
               /\ pc' = [pc EXCEPT ![self] = "whil_"]
               /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, colB, 
                               i_, j_, result, h, i, j >>

send(self) == /\ pc[self] = "send"
              /\ sendB' = [sendB EXCEPT ![self-N] = colB[self]]
              /\ pc' = [pc EXCEPT ![self] = "Done"]
              /\ UNCHANGED << sendA, sendC, C, produit, finished, lB, colB, i_, 
                              j_, result, h, i, j >>

PB(self) == init(self) \/ whil_(self) \/ affe_(self) \/ incr_(self)
               \/ send(self)

wait_(self) == /\ pc[self] = "wait_"
               /\ {a \in 1..M : sendA[i_[self]][a] = 0} = {} /\ {a \in 1..M : sendB[j_[self]][a] = 0} = {}
               /\ pc' = [pc EXCEPT ![self] = "whil"]
               /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                               colB, i_, j_, result, h, i, j >>

whil(self) == /\ pc[self] = "whil"
              /\ IF h[self] <= M
                    THEN /\ pc' = [pc EXCEPT ![self] = "calc"]
                    ELSE /\ pc' = [pc EXCEPT ![self] = "affe_C"]
              /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                              colB, i_, j_, result, h, i, j >>

calc(self) == /\ pc[self] = "calc"
              /\ result' = [result EXCEPT ![self] = result[self] + sendA[i_[self]][h[self]]*sendB[j_[self]][h[self]]]
              /\ pc' = [pc EXCEPT ![self] = "incr"]
              /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                              colB, i_, j_, h, i, j >>

incr(self) == /\ pc[self] = "incr"
              /\ h' = [h EXCEPT ![self] = h[self]+1]
              /\ pc' = [pc EXCEPT ![self] = "whil"]
              /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                              colB, i_, j_, result, i, j >>

affe_C(self) == /\ pc[self] = "affe_C"
                /\ sendC' = [sendC EXCEPT ![(i_[self]-1)*N+j_[self]] = result[self]]
                /\ pc' = [pc EXCEPT ![self] = "Done"]
                /\ UNCHANGED << sendA, sendB, C, produit, finished, lB, colB, 
                                i_, j_, result, h, i, j >>

CALCUL(self) == wait_(self) \/ whil(self) \/ calc(self) \/ incr(self)
                   \/ affe_C(self)

wait_P(self) == /\ pc[self] = "wait_P"
                /\ sendC[(i[self]-1)*N+j[self]] # 0
                /\ pc' = [pc EXCEPT ![self] = "affe"]
                /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                                colB, i_, j_, result, h, i, j >>

affe(self) == /\ pc[self] = "affe"
              /\ C' = [C EXCEPT ![i[self]][j[self]] = sendC[(i[self]-1)*N+j[self]]]
              /\ pc' = [pc EXCEPT ![self] = "wend"]
              /\ UNCHANGED << sendA, sendB, sendC, produit, finished, lB, colB, 
                              i_, j_, result, h, i, j >>

wend(self) == /\ pc[self] = "wend"
              /\ {a \in 1..N*P : C[(a+1) \div 2][((a+1)%2)+1] = 0} = {}
              /\ pc' = [pc EXCEPT ![self] = "fini"]
              /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, 
                              colB, i_, j_, result, h, i, j >>

fini(self) == /\ pc[self] = "fini"
              /\ finished' = 1
              /\ pc' = [pc EXCEPT ![self] = "Done"]
              /\ UNCHANGED << sendA, sendB, sendC, C, produit, lB, colB, i_, 
                              j_, result, h, i, j >>

PC(self) == wait_P(self) \/ affe(self) \/ wend(self) \/ fini(self)

wait == /\ pc[0] = "wait"
        /\ finished=1
        /\ pc' = [pc EXCEPT ![0] = "disp"]
        /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, colB, 
                        i_, j_, result, h, i, j >>

disp == /\ pc[0] = "disp"
        /\ PrintT(<<C>>)
        /\ pc' = [pc EXCEPT ![0] = "Done"]
        /\ UNCHANGED << sendA, sendB, sendC, C, produit, finished, lB, colB, 
                        i_, j_, result, h, i, j >>

PRINT == wait \/ disp

Next == PRINT
           \/ (\E self \in 1..N: PA(self))
           \/ (\E self \in N+1..N+P: PB(self))
           \/ (\E self \in N+P+1..N+P+N*P: CALCUL(self))
           \/ (\E self \in N+P+N*P+1..N+P+N*P*2: PC(self))
           \/ (* Disjunct to prevent deadlock on termination *)
              ((\A self \in ProcSet: pc[self] = "Done") /\ UNCHANGED vars)

Spec == /\ Init /\ [][Next]_vars
        /\ WF_vars(Next)

Termination == <>(\A self \in ProcSet: pc[self] = "Done")

\* END TRANSLATION



============

